var context;   
var type;      
var x, y;   
var orientation;      //Ausrichtung
var fields;   //Array
var timer;  
var timestep;  

function init() 
{
    c = document.getElementById("myCanvas");
    context = c.getContext("2d");
     
    //Random Auswahl des Typs
    type = 1 + Math.floor((Math.random()*7));
    x = 4;
    y = 18;
    orientation = 0;
     
    //Leeres fields erstellen
    fields = new Array(20);
    for(i = 0; i < 20; i++) {
        fields[i] = new Array(10);
        for(j = 0; j < 10; j++)
            fields[i][j] = 0;
    }
     
    //Tetris Typ zeichnen
    drawType(x,y,type,orientation,1);
     
    //Feld neu zeichnen
    drawFields();

    timestep = 800;
     
    //Start the game timer
    clearInterval(timer);
    timer = setInterval(function(){gameLoop()}, timestep);
}
 
 

function drawFields() {
     
    //Kontext Clearen
    context.clearRect(0,0,200,400);
     
    for(i = 0; i < 20; i++) 
    {
        for(j = 0; j < 10; j++)
        {
        	drawRect(j, i, fields[i][j]);
        }      
    }
}
  
function drawRect(x, y, type) {
     
    var color;
    
    //Durch mitgegeber Typinfo Farbe einstellen
    if(type > 0) {
     	switch (type)
     	{
     		case 1: 
     			context.fillStyle = "#FF0080";	//I Block PINK
     			break;
     		case 2: 
     			context.fillStyle = "#FFCF00";	//J Block GELB
     			break;
     		case 3: 
     			context.fillStyle = "#AF4FA2";	//L Block LILA
     			break;
     		case 4: 
     			context.fillStyle = "#C5E946";	//orientation Block HELLGRÜN
     			break;
     		case 5: 
     			context.fillStyle = "#0033CC";	//S Block BLAU
     			break;
     		case 6: 
     			context.fillStyle = "#FF0000";	//type Block ROT
     			break;
     		default: 
     			context.fillStyle = "#669900";	//Z Block DUNKELGRÜN
     			break;
     	}
     
        //Pixelkoordinaten ausrechen
        pX = x*20;
        pY = (19-y)*20;
           
        //Rechteck Erzeugen 
        context.fillRect(pX+2,pY+2,18,18);
    }
}
   
function drawType(x,y,type,orientation,d) 
{ 
    c = -1;
    if(d >= 0) c = type*d;
 
    valid = true;
 
    if(type == 1) //I Type
    { 
        if(orientation == 0 || orientation == 2) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x+2,y,c);
        }
        else if(orientation == 1 || orientation == 3) 
        {
            valid = valid && setGrid(x+1,y+1,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x+1,y-1,c);
            valid = valid && setGrid(x+1,y-2,c);
        }
    }
    if(type == 2) //J Type
    { 

        if(orientation == 0) 
        {
            valid = valid && setGrid(x-1,y+1,c);
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
        }
        else if(orientation == 1) 
        {
            valid = valid && setGrid(x+1,y+1,c);
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
        }
        else if(orientation == 2) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x+1,y-1,c);
        }
        else if(orientation == 3) 
        {
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
            valid = valid && setGrid(x-1,y-1,c);
        }
    }
    if(type == 3) //L Type
    { 
        if(orientation == 0) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x+1,y+1,c);
        }
        else if(orientation == 1) 
        {
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
            valid = valid && setGrid(x+1,y-1,c);
        }
        else if(orientation == 2) 
        {
            valid = valid && setGrid(x-1,y-1,c);
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
        }
        else if(orientation == 3) 
        {
            valid = valid && setGrid(x-1,y+1,c);
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
        }
    }
    if(type == 4) // O Type
    { 
        valid = valid && setGrid(x,y,c);
        valid = valid && setGrid(x+1,y,c);
        valid = valid && setGrid(x,y+1,c);
        valid = valid && setGrid(x+1,y+1,c);
    }
    if(type == 5) //S Type
    { 
        if(orientation == 0 || orientation == 2) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x+1,y+1,c);
        }
        else if(orientation == 1 || orientation == 3) 
        {
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x+1,y-1,c);
        }
    }
    if(type == 6) //T Type
    {         
        if(orientation == 0) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x,y+1,c);
        }
        else if(orientation == 1) 
        {
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
            valid = valid && setGrid(x+1,y,c);
        }
        else if(orientation == 2) 
        {
            valid = valid && setGrid(x-1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x,y-1,c);
        }
        else if(orientation == 3) 
        {
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
            valid = valid && setGrid(x-1,y,c);
        }
    }
    if(type == 7) // Z Type
    {      
        if(orientation == 0 || orientation == 2) 
        {
            valid = valid && setGrid(x-1,y+1,c);
            valid = valid && setGrid(x,y+1,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x+1,y,c);
        }
        else if(orientation == 1 || orientation == 3) 
        {
            valid = valid && setGrid(x+1,y+1,c);
            valid = valid && setGrid(x+1,y,c);
            valid = valid && setGrid(x,y,c);
            valid = valid && setGrid(x,y-1,c);
        }
    }
     
    return valid;
}

function setGrid(x, y, type) {
     
   
    if(x >= 0 && x < 10 && y >= 0 && y < 20) 
    {
        if(type < 0) 
        {
        	return fields[y][x] == 0;
        }	
        else
        {
        	fields[y][x] = type;
        	return true;
        } 
    }
    return false;
}
  
function keyDown(event) 
{
     
    if(event.keyCode == 37) // Linker Pfeil 
    {
        drawType(x,y,type,orientation,0);  
        x2 = x - 1;
        if(drawType(x2,y,type,orientation,-1)) 
        {
        	x = x2;
        }
            
    }
    else if(event.keyCode == 38) // Hoch Pfeil
    { 
    	// Orientierung ändern
        drawType(x,y,type,orientation,0); 
        orientation2 = (orientation + 1) % 4;
        if(drawType(x,y,type,orientation2,-1))
        {
        	orientation = orientation2;
        }            
    }
    else if(event.keyCode == 39) // Rechter Pfeil
    {
        drawType(x,y,type,orientation,0);
        x2 = x + 1;
        if(drawType(x2,y,type,orientation,-1))
        {
        	x = x2;
        }
           
    }
    else if(event.keyCode == 40) // Runter Pfeil 
    { 
        drawType(x,y,type,orientation,0);  
        y2 = y - 1;
        if(drawType(x,y2,type,orientation,-1)) 
        {
        	y = y2;
        }   
    }
      
    
    drawType(x,y,type,orientation,1);
    drawFields();
}
 
function gameLoop() 
{  
    drawType(x,y,type,orientation,0); 
     
    y2 = y - 1;
    if(drawType(x,y2,type,orientation,-1))
    {
    	y = y2;
    }       
    else 
    {
        drawType(x,y,type,orientation,1);
     
        // Auf volle Linien überprüfen
        checkLines();
     
        //Neue Figur erzeugen
        type2 = 1 + Math.floor((Math.random()*7));
        x2 = 4;
        y2 = 18;
        orientation2 = 0;
         
        if(drawType(x2,y2,type2,orientation2,-1)) 
        {
            type = type2;
            x = x2;
            y = y2;
            orientation = orientation2;
        }
        else 
        {
        	// GameOver und Neustarten
            alert("Game Over");
            init();
            return;
        }
         
    }

    drawType(x,y,type,orientation,1);
    drawFields();
}
 
function checkLines() 
{
    for(i = 0; i < 20; i++) 
    { 
        full = true;
        for(j = 0; j < 10; j++)
        {
        	full = full && (fields[i][j] > 0);
        }
                    
        if(full) 
        {

            //Loop over the remaining lines
            for(ii = i; ii < 19; ii++) 
            {
                for(j = 0; j < 10; j++)
                {
                	fields[ii][j] = fields[ii+1][j];
                }      
            }
             
            for(j = 0; j < 10; j++)
            {
            	fields[19][j] = 0;
            }

            i--;
        }
    }
}

